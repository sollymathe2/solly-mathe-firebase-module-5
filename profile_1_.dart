Import package:flutter/material.dart;
import package:firebase_core/firebase_core.dart;
Import package:flutter_app/menu.dart;
Import package:flutter_app/profile.dart;
Import package:flutter_app/settings.dart;
Import package:flutter_app/about.dart;

Main() {
  widgetsflutterbinding.ensureinitialized();
  runApp(
      MaterialApp(
    Home: MyApp(),
    final future<firebaseApp> _initializatio =firebase.initiolizeApp();
  ));
}

home: futurebuilder(
  future:(context,snapshot){
    if (snapshot.hasError){
      print("Error");
    }
    if (snapshot.connectionstate.done)
  }
)
Class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    Return Scaffold(
      appBar: AppBar(
        title: Text(DashBoard),
      ),
      Body: Center(
          Child: Column(
        Children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  Padding: EdgeInsets.all(20.0),
                  Child: new MaterialButton(
                    Height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: new Text(Profile),
                    onPressed: () => {
                    Navigator.push(
                    Context,
                    MaterialPageRoute(builder: (context) => Profile()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
              Padding(
                  Padding: EdgeInsets.all(20.0),
                  Child: new MaterialButton(
                    Height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: new Text(Menu),
                    onPressed: () => {
                    Navigator.push(
                    Context,
                    MaterialPageRoute(builder: (context) => Menu()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  Padding: EdgeInsets.all(20.0),
                  Child: new MaterialButton(
                    Height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: new Text(Settings),
                    onPressed: () => {
                    Navigator.push(
                    Context,
                    MaterialPageRoute(builder: (context) => Settings()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
              Padding(
                  Padding: EdgeInsets.all(20.0),
                  Child: new MaterialButton(
                    Height: 100.0,
                    minWidth: 150.0,
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    child: new Text(About),
                    onPressed: () => {
                    Navigator.push(
                    Context,
                    MaterialPageRoute(builder: (context) => About()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
            ],
          ),

        ],
      )),
    );
  }
